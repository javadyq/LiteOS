;/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
;* File Name          : startup_stm32f40_41xxx.s
;* Author             : MCD Application Team
;* @version           : V1.6.0
;* @date              : 10-July-2015
;* Description        : STM32F40xxx/41xxx devices vector table for EWARM toolchain.
;*                      This module performs:
;*                      - Set the initial SP
;*                      - Set the initial PC == _iar_program_start,
;*                      - Set the vector table entries with the exceptions ISR
;*                        address.
;*                      - Configure the system clock and the external SRAM mounted on
;*                        STM324xG-EVAL board to be used as data memory (optional,
;*                        to be enabled by user)
;*                      - Branches to main in the C library (which eventually
;*                        calls main()).
;*                      After Reset the Cortex-M4 processor is in Thread mode,
;*                      priority is Privileged, and the Stack is set to Main.
;********************************************************************************
;*
;* Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
;* You may not use this file except in compliance with the License.
;* You may obtain a copy of the License at:
;*
;*        http://www.st.com/software_license_agreement_liberty_v2
;*
;* Unless required by applicable law or agreed to in writing, software
;* distributed under the License is distributed on an "AS IS" BASIS,
;* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;* See the License for the specific language governing permissions and
;* limitations under the License.
;*
;*******************************************************************************/
;
;
; The modules in this file are included in the libraries, and may be replaced
; by any user-defined modules that define the PUBLIC symbol _program_start or
; a user defined start symbol.
; To override the cstartup defined in the library, simply add your modified
; version to the workbench project.
;
; The vector table is normally located at address 0.
; When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
; The name "__vector_table" has special meaning for C-SPY:
; it is where the SP start value is found, and the NVIC vector
; table register (VTOR) is initialized to this address if != 0.
;
; Cortex-M version
;

LOS_Heap_Min_Size   EQU     0x400

                AREA    LOS_HEAP, NOINIT, READWRITE, ALIGN=3
__los_heap_base
LOS_Heap_Mem    SPACE   LOS_Heap_Min_Size


                AREA    LOS_HEAP_INFO, DATA, READONLY, ALIGN=2
                IMPORT  |Image$$ARM_LIB_STACKHEAP$$ZI$$Base|
                EXPORT  __LOS_HEAP_ADDR_START__
                EXPORT  __LOS_HEAP_ADDR_END__
__LOS_HEAP_ADDR_START__
                DCD     __los_heap_base
__LOS_HEAP_ADDR_END__
                DCD     |Image$$ARM_LIB_STACKHEAP$$ZI$$Base| - 1


                PRESERVE8

                AREA    RESET, CODE, READONLY
                THUMB

                IMPORT  ||Image$$ARM_LIB_STACKHEAP$$ZI$$Limit||

                EXPORT  _BootVectors
                EXPORT  Reset_Handler

_BootVectors
                DCD     ||Image$$ARM_LIB_STACKHEAP$$ZI$$Limit||
                DCD     Reset_Handler


Reset_Handler
                LDR.W   R0, =0xE000ED88
                LDR     R1, [R0]
                ORR     R1, R1, #(0xF << 20)
                STR     R1, [R0]
                CPSID   I

                IMPORT  SystemInit
                IMPORT  __main
                LDR     R0, =SystemInit
                BLX     R0
                LDR     R0, =__main
                BX      R0


                ALIGN
                END

